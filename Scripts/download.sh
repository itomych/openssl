#!/bin/bash
set -ev

echo "fetching openssl"
curl -OL https://bitbucket.org/itomych/openssl/raw/master/downloads/openssl.zip

unzip -o -q openssl.zip
rm openssl.zip
